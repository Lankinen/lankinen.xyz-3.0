import { ReactNode } from 'react';
import { Col } from '@lankinen/component-lab';
import useWindowSize from 'hooks/useWindowSize';
import { CSS } from '@stitches/react';

const BIGW = 300;
const BIGH = 250;
const SMALLW = 150;
const SMALLH = 125;

interface Props {
  size: 'small' | 'medium' | 'big';
  height?: number | string;
  color: string;
  rgba?: string;
  onClick?: () => void;
  css?: CSS;
  children: ReactNode;
}

function WidgetWrapper(props: Props) {
  const { size, height, rgba, color, onClick = null, css, children } = props;
  const windowSize = useWindowSize();

  const multiplier = windowSize.width
    ? Math.min(windowSize.width + 200, 1000) / 1000
    : 0;
  let width;
  let minHeight;
  if (size === 'small') {
    width = SMALLW * multiplier;
    minHeight = SMALLH * multiplier;
  } else if (size === 'big') {
    width = BIGW * multiplier;
    minHeight = BIGH * multiplier;
  } else {
    width = BIGW * multiplier;
    minHeight = SMALLH * multiplier;
  }

  return (
    <Col
      css={{
        width,
        minHeight,
        height,
        borderRadius: 10,
        padding: 10,
        background: `linear-gradient(transparent, ${rgba})`,
        backgroundColor: color,
        ...(onClick ? { cursor: 'pointer' } : {}),
        ...css,
      }}
      onClick={onClick || (() => null)}
    >
      {children}
    </Col>
  );
}

export default WidgetWrapper;
