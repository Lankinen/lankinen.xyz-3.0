import useSWR from 'swr';
import Tooltip from 'rc-tooltip';
import { Text, Row } from '@lankinen/component-lab';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from 'hooks/useWindowSize';

interface CalendarItemProps {
  name: string;
  minutes: number;
  description: string;
  backgroundColor?: string;
  multiplier: number;
}

function CalendarItem(props: CalendarItemProps) {
  const {
    name,
    minutes: _minutes,
    description,
    backgroundColor,
    multiplier,
  } = props;

  const hours = Math.floor(_minutes / 60);
  const minutes = _minutes - hours * 60;
  const twoDigitMinutes = minutes.toLocaleString('en-US', {
    minimumIntegerDigits: 2,
    useGrouping: false,
  });

  return (
    <Row css={{ justifyContent: 'space-between' }}>
      <Tooltip
        overlay={description}
        placement="left"
        mouseEnterDelay={0.1}
        overlayStyle={{ maxWidth: 300 }}
      >
        <Text
          style={{
            fontSize: 16 * multiplier,
            cursor: 'help',
            color: backgroundColor ?? 'white',
          }}
        >
          {name}
        </Text>
      </Tooltip>
      <Text
        style={{ fontSize: 16 * multiplier, color: backgroundColor ?? 'white' }}
      >
        {hours ? `${hours} h ${twoDigitMinutes} min` : `${twoDigitMinutes} min`}
      </Text>
    </Row>
  );
}

function CalendarWidget() {
  const windowSize = useWindowSize();

  const multiplier = windowSize.width
    ? Math.min(windowSize.width + 300, 900) / 900
    : 0;

  const { data, error } = useSWR('/api/calendar', async (url) => {
    const response = await fetch(url);
    const data: Record<
      string,
      { minutes: number; description: string; backgroundColor?: string }
    > = await response.json();
    const sortedData = Object.entries(data).sort(
      (a, b) => b[1].minutes - a[1].minutes
    );
    return sortedData;
  });

  if (error || !data) {
    return null;
  }
  return (
    <WidgetWrapper
      color="rgba(50,50,50,0.90)"
      size={windowSize.width < 600 ? 'big' : 'medium'}
    >
      <Tooltip
        overlay="This is how I spent my last week (Monday to Sunday). The data is pulled from my Google Calendar where I track everything I do within 15 min accuracy. I don't allow overlapping events (e.g. listening podcast and walking) but I mark them under the main activity. I share this information to keep myself accountable to spend my time on activites that take me closer to my goals rather than activites that make me happy in short term."
        placement="left"
        mouseEnterDelay={0.1}
        overlayStyle={{ maxWidth: 300 }}
      >
        <Text
          css={{ fontSize: 24 * multiplier, cursor: 'help', color: 'white' }}
        >
          Calendar - Last Week
        </Text>
      </Tooltip>
      {data.map(([name, { minutes, description, backgroundColor }]) => (
        <CalendarItem
          key={name}
          name={name}
          minutes={minutes}
          description={description}
          backgroundColor={backgroundColor}
          multiplier={multiplier}
        />
      ))}
    </WidgetWrapper>
  );
}

export default CalendarWidget;
