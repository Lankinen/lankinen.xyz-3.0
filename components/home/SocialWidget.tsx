import React from 'react';
import { Box, Img, Text } from '@lankinen/component-lab';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from 'hooks/useWindowSize';

interface Props {
  icon: string;
  text: string;
  rgba: string;
  color: string;
  url: string;
}

function SocialWidget(props: Props) {
  const { icon, text, rgba, color, url } = props;
  const windowSize = useWindowSize();

  const multiplier = windowSize.width
    ? Math.min(windowSize.width + 150, 900) / 900
    : 0;

  return (
    <Box
      css={{
        padding: 20 * multiplier,
      }}
    >
      <WidgetWrapper
        size="small"
        rgba={rgba}
        color={color}
        onClick={() => window.open(url, '_blank')}
      >
        <Box
          css={{
            position: 'absolute',
            bottom: 10 * multiplier,
            pl: 10 * multiplier,
          }}
        >
          <Img
            src={icon}
            alt="icon"
            style={{ width: 60 * multiplier, height: 60 * multiplier }}
          />
          <Text
            css={{
              mt: 8 * multiplier,
              fontSize: 16 * multiplier,
              fontWeight: 'bold',
              color: 'white',
            }}
          >
            {text}
          </Text>
        </Box>
      </WidgetWrapper>
    </Box>
  );
}

export default SocialWidget;
