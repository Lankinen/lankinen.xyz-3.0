import React, { useEffect, useState } from 'react';
import { Row, Text, Box } from '@lankinen/component-lab';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from 'hooks/useWindowSize';
import useGhostWriter from 'hooks/useGhostWriter';

function GhostWriter({ message }: { message: any }) {
  const writing = useGhostWriter(message);

  return <Text>{writing}</Text>;
}

function HelloWidget() {
  const windowSize = useWindowSize();

  const getTime = () =>
    new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

  const [time, setTime] = useState(getTime());
  useEffect(() => {
    setInterval(() => {
      setTime(getTime());
    }, 10000);
  }, []);

  const [joke, setJoke] = useState<string[] | undefined>();
  useEffect(() => {
    (async () => {
      const response = await fetch('/api/jokes');
      const data = await response.json();
      setJoke(data);
    })();
  }, []);

  const [quote, setQuote] = useState('');
  useEffect(() => {
    (async () => {
      const response = await fetch('/api/quotes');
      const quote = await response.text();
      setQuote(quote);
    })();
  }, []);

  const multiplier = windowSize.width
    ? Math.min(windowSize.width + 300, 900) / 900
    : 0;

  return (
    <WidgetWrapper
      color="rgba(220,220,220,0.92)"
      size={windowSize.width < 600 ? 'big' : 'medium'}
    >
      <Box
        css={{
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <Row>
          {joke && (
            <GhostWriter
              message={[
                ['Wanna hear a joke?', 2000],
                [joke[0], 1500],
                [joke[1], 1000],
                ["Hope you like it. Here's a random quote.", 1000],
                [quote, 100],
              ]}
            />
          )}
        </Row>
        <Text style={{ fontSize: 16 * multiplier }}>{time}</Text>
      </Box>
    </WidgetWrapper>
  );
}

export default HelloWidget;
