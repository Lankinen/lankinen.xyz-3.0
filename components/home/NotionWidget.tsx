import useSWR from 'swr';
import { Text, Row } from '@lankinen/component-lab';
import WidgetWrapper from './WidgetWrapper';
import useWindowSize from 'hooks/useWindowSize';
import { Book } from 'pages/api/notion';

export default function NotionWidget() {
  const windowSize = useWindowSize();

  const multiplier = windowSize.width
    ? Math.min(windowSize.width + 300, 900) / 900
    : 0;

  const { data, error } = useSWR('/api/notion', async (url) => {
    const response = await fetch(url);
    const data: Book[] = await response.json();
    return data;
  });

  if (error || !data?.length) {
    return null;
  }
  return (
    <WidgetWrapper
      color="rgba(50,50,50,0.90)"
      size="medium"
      height="fit-content"
    >
      <Text css={{ color: 'White', fontSize: 20 * multiplier }}>
        The last 5 books I read
      </Text>
      {data.map(({ name, url }) => (
        <Row key={url}>
          <Text
            as="a"
            target="_blank"
            rel="noreferrer"
            href={url}
            css={{ fontSize: 14 * multiplier, color: 'LimeGreen', py: 4 }}
          >
            {name}
          </Text>
        </Row>
      ))}
    </WidgetWrapper>
  );
}
