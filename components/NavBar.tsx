import { Row, Span } from '@lankinen/component-lab';
import Link from 'next/link';
import { useRouter } from 'next/router';

interface LinkItemProps {
  children: string;
  path: string;
}

function LinkItem(props: LinkItemProps) {
  const { children, path } = props;
  const router = useRouter();

  const isSelected =
    path === '/'
      ? router.pathname === '/'
      : router.pathname.split('/').includes(path.split('/').join(''));

  return (
    <Span
      css={{
        cursor: 'pointer',
        borderRadius: 3,
        px: 5,
        backgroundColor: isSelected ? 'lightgray' : undefined,
        '&:hover': {
          backgroundColor: 'lightgray',
          transition: 'background-color 100ms linear',
        },
      }}
    >
      <Link href={path} passHref>
        <Span css={{ color: 'black' }}>{children}</Span>
      </Link>
    </Span>
  );
}

const PAGES: { path: string; name: string }[] = [
  // { path: '/', name: 'Dashboard' },
  { path: '/projects', name: 'Projects' },
  { path: '/about', name: 'About' },
  { path: '/contact', name: 'Contact' },
];

interface Props {
  height: string;
}

export default function NavBar(props: Props) {
  const { height } = props;

  // useEffect(() => {
  //   const handleKeyDown = (event: KeyboardEvent) => {
  //     switch (event.code) {
  //       case 'F1':
  //         router.push(PAGES[0].path);
  //         break;
  //       case 'F2':
  //         router.push(PAGES[1].path);
  //         break;
  //       case 'F3':
  //         router.push(PAGES[2].path);
  //         break;
  //       case 'F4':
  //         router.push(PAGES[3].path);
  //         break;
  //       default:
  //         return;
  //     }
  //     event.preventDefault();
  //   };

  //   document.addEventListener('keydown', handleKeyDown);

  //   return () => document.removeEventListener('keydown', handleKeyDown);
  // }, [router]);

  return (
    <Row
      css={{
        height,
        borderBottom: '1px solid grey',
        gap: 8,
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onKeyDown={(event: React.KeyboardEvent<HTMLDivElement>) => {
        console.log(event.code);
      }}
    >
      {PAGES.map((page) => (
        <LinkItem key={page.name} path={page.path}>
          {page.name}
        </LinkItem>
      ))}
    </Row>
  );
}
