import { Col, Img, Text } from '@lankinen/component-lab';
import Link from 'next/link';
import { CSS } from 'stitches.config';

interface Props {
  img: string;
  name?: string;
  to?: string;
  year?: number | string;
  size?: number;
  css?: CSS;
}

export default function AppIcon(props: Props) {
  const { name, to, year, img, size = 100, css } = props;

  return (
    <Col css={css}>
      {to && to[0] === '/' ? (
        <Link href={to} passHref>
          <a>
            <Icon img={img} size={size} showPointer />
          </a>
        </Link>
      ) : (
        <a href={to} target="_blank" rel="noreferrer">
          <Icon img={img} size={size} showPointer={!!to} />
        </a>
      )}
      {name && (
        <Text css={{ maxWidth: size, fontWeight: 'bold', px: 5 }}>
          {name}, {year}
        </Text>
      )}
    </Col>
  );
}

interface IconProps {
  img: string | undefined;
  size: number;
  showPointer?: boolean;
}

function Icon(props: IconProps) {
  const { img, size, showPointer } = props;
  return (
    <Img
      src={`/images/projects/${img || 'profile_pic_simp.png'}`}
      alt="project"
      css={{
        borderRadius: 10,
        overflow: 'hidden',
        width: size,
        height: size,
        cursor: showPointer ? 'pointer' : 'default',
      }}
    />
  );
}
