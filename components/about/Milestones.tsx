import { WindowsParagraph, WindowsHeader } from './Components';

function Milestones() {
  return (
    <>
      {/* 2000 */}
      <WindowsHeader as="h2">2000 - Birth</WindowsHeader>
      {/* 2008 */}
      <WindowsHeader as="h2">2008 - Inventor</WindowsHeader>
      <WindowsParagraph>
        This is the second serious dream profession I had (the first was
        entomologist) and the beginning of the journey I am on right now. I got inspiration from a cartoon series{' '}
        <a
          href="https://en.wikipedia.org/wiki/Once_Upon_a_Time..._The_Discoverers"
          target="_blank"
          rel="noreferrer"
        >
          Once Upon a Time... The Discoverers
        </a>
        . Solving problems and making life easier was something I naturally wanted to do.
      </WindowsParagraph>
      {/* 2010 */}
      <WindowsHeader as="h2">2010 - Entrepreneur</WindowsHeader>
      <WindowsParagraph>
        I started watching{' '}
        <a
          href="https://en.wikipedia.org/wiki/Shark_Tank"
          target="_blank"
          rel="noreferrer"
        >
          Shark Tank
        </a> which made me
        realize that I didn’t actually want to just be an inventor but an
        entrepreneur. Entrepreneurs go further than inventors getting more
        credits and money from their inventions.
      </WindowsParagraph>
      {/* 2013 */}
      <WindowsHeader as="h2">2013 - Coder</WindowsHeader>
      <WindowsParagraph>
        After spending a few years building all kinds of small devices from different
        materials and components, I realized that there were always limitations to what I could
        build because I didn’t have the right tools, materials, or space. I knew
        what coding was and I wanted to learn it for a few years but I didn’t
        know where to start and when I tried to start somewhere (once I tried to
        read a 600 pages long book about Java), it didn’t lead anywhere.
      </WindowsParagraph>
      <WindowsParagraph>
        This was the point when I realized that I couldn’t anymore procrastinate
        on this dream but I needed to do whatever it takes and no matter how
        long it takes. The turning point was when I found material for a course
        in a local university (the same university where 6 years later I went to
        study CS).
      </WindowsParagraph>
      <WindowsParagraph>
        It took me almost a year before I started to feel that I could actually
        do things I wanted and didn’t just need to follow instructions. It was
        painful for the whole year and I had moments when I was ready to quit but this is one of my favorite achievements because I kept going.
      </WindowsParagraph>
      {/* 2014 */}
      <WindowsHeader as="h2">2014 - Mobile Games</WindowsHeader>
      <WindowsParagraph>
        The first real programs I started doing were mobile games. Mainly
        because I’m from Finland and there were quite many{' '}
        <a
          href="https://en.wikipedia.org/wiki/Video_games_in_Finland"
          target="_blank"
          rel="noreferrer"
        >
          successful mobile gaming companies
        </a>{' '}
        so I believed I could also make millions (or even billions).
      </WindowsParagraph>
      <WindowsParagraph>
        I spent about two years actively just building one game after another
        improving every time. I was the kind of person who liked abandoning
        ideas when they didn’t work so I often released the first version and
        when it didn’t get any downloads, I started from scratch with another
        idea. Some say that it might be a good way to work [1] but I also felt
        being a failure when I quit so quickly.
      </WindowsParagraph>
      <WindowsParagraph>
        I didn’t make any money or get many downloads but I learned a lot by
        iterating over and over. I am lucky to be optimistic person because
        the hope of success was a big motivator to keep me going after failures.
      </WindowsParagraph>
      <WindowsParagraph css={{ fontSize: 'smaller' }}>
        [1] “Maybe it would be good for hackers to act more like painters, and
        regularly start over from scratch, instead of continuing to work for
        years on one project, and trying to incorporate all their later ideas as
        revisions.” http://www.paulgraham.com/hp.html
      </WindowsParagraph>
      {/* 2017 */}
      <WindowsHeader as="h2">2017 - AI</WindowsHeader>
      <WindowsParagraph>
        When I stopped playing mobile games, I felt it didn’t anymore make sense
        for me to develop them. I believe it’s crucial to eat your own dog food.
        The programming I had done was pretty straightforward and I was
        surprised that I didn’t really need math unlike my impression of coding
        has been before learning it.
      </WindowsParagraph>
      <WindowsParagraph>
        I started to think about what were some challenging areas where I could
        improve my programming skills and perhaps grasp some new ones. I came by
        a Forbes article about a person who was introduced to AI when he saw his
        college roommate showing a fire simulation that used AI. I knew the term
        but I didn’t have any idea how it actually worked.
      </WindowsParagraph>
      <WindowsParagraph>
        I started by taking Andrew Ng’s famous machine learning course (is there any other
        way to start learning it) and then continued to some other courses.
      </WindowsParagraph>
      <WindowsParagraph>
        It was perfect timing because I learned at the same time in high school
        math the same methods that were used in machine learning. I kept
        learning it actively for a year and built, too little bit still, some
        small programs.
      </WindowsParagraph>
      {/* 2019 */}
      <WindowsHeader>2019 - University</WindowsHeader>
      <WindowsParagraph>
        Since I started watching Shark Tank, I knew that my end goal was to
        build my own businesses. One big decision was to decide whether I wanted
        to study business or computer science at university. For some time I
        thought about going to study business but then I realized that business
        isn’t the hardest part. Many startup CEOs weren’t business people but
        programmers.
      </WindowsParagraph>
      <WindowsParagraph>
        I ended up going to study computer science and decided that I could
        study business on my own. Being somewhat experienced, I was available to
        get a bachelor&apos;s degree in 2 years when the target time is 3 and
        for some, it takes 4.
      </WindowsParagraph>
      <WindowsParagraph>
        Overall, for me university was valuable and I don’t regret going there.
        I had some holes in my knowledge being self-learned without a
        curriculum. Filling those holes and just getting to see what it was like
        to study programming in a formal environment was useful.
      </WindowsParagraph>
      {/* 2021 */}
      <WindowsHeader as="h2">2021 - Software Engineer</WindowsHeader>
      <WindowsParagraph>
        I got to my “first job” (I kind of had a job before this) when I was 6
        months away from graduation. I had slightly fewer courses so I was
        available to work full time and then study during evenings and weekends.
        The company was a local startup and I learned some important lessons
        there. Some of the most valuable lessons were what to not do rather than
        what to do.
      </WindowsParagraph>
      <WindowsParagraph>
        My plan was to continue to study master&apos;s degree and work on side.
        The plan changed when I got to a new job. It was a Silicon Valley based
        startup that had been part of YC (for me it indicates quality). Almost
        immediately I realized that I could use my time much better there than
        studying master&apos;s degree so I decided to dedicate more time to it
        and not pursue a master’s degree.
      </WindowsParagraph>
    </>
  );
}

export default Milestones;
