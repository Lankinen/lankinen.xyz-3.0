import { Box } from '@lankinen/component-lab';
import { useRouter } from 'next/router';
import { WindowsText } from '../Components';

interface TabProps {
  isActive: boolean;
  resetFakeWait: () => void;
  children: string;
}

export default function Tab(props: TabProps) {
  const { isActive, resetFakeWait, children } = props;
  const router = useRouter();

  return (
    <Box
      css={{
        position: 'relative',
        top: isActive ? 4 : 0,
        height: isActive ? 38 : 30,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        width: 'fit-content',
        backgroundColor: 'rgba(198,198,198)',
        pb: 8,
        px: 8,
        pt: 5,
        boxShadow:
          '4px 4px 0px 0px #EBEBEB inset, -4px 0px 0px 0px #0A0A0A inset',
      }}
    >
      <Box
        as="button"
        css={{
          cursor: 'pointer',
          px: isActive ? 5 : 0,
          pt: isActive ? 5 : 0,
          '&:focus': isActive
            ? {
                border: '2px dotted black',
                px: 3,
                pt: 3,
              }
            : undefined,
          border: 0,
          backgroundColor: 'transparent',
        }}
        onClick={() => {
          resetFakeWait();
          router.push(`/about/${children.toLowerCase()}`);
        }}
      >
        <WindowsText>{children}</WindowsText>
      </Box>
    </Box>
  );
}
