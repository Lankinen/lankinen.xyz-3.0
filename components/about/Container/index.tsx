import { Box, Row } from '@lankinen/component-lab';
import useFakeWait from 'hooks/useFakeWait';

import Hourglass from '../Hourglass';
import Me from '../Me';
import Milestones from '../Milestones';
import Error from '../Error';
import Tab from './Tab';
import { WindowsText } from '../Components';

interface Props {
  activeTab: string;
}

export default function Container(props: Props) {
  const { activeTab } = props;

  const { fakeWait, resetFakeWait } = useFakeWait();

  const Content = () => {
    if (fakeWait) {
      return <Hourglass size={32} />;
    }
    switch (activeTab) {
      case 'me':
        return <Me />;
      case 'milestones':
        return <Milestones />;
      default:
        return <Error />;
    }
  };

  return (
    <Box
      css={{
        width: '100%',
        minHeight: '100%',
        backgroundColor: 'rgba(198,198,198)',
        p: 8,
        boxShadow:
          '4px 4px 0px 0px #EBEBEB inset, -4px -4px 0px 0px #0A0A0A inset',
      }}
    >
      <Box css={{ backgroundColor: 'rgb(6, 0, 132)', mb: 10 }}>
        <WindowsText
          css={{
            color: 'white',
            pl: 5,
            fontWeight: 'bolder',
            fontSize: 18,
          }}
        >
          🖥️ lankinen.exe
        </WindowsText>
      </Box>
      <Box>
        <Row css={{ alignItems: 'flex-end' }}>
          <Box css={{ ml: 10 }}>
            <Tab isActive={activeTab === 'me'} resetFakeWait={resetFakeWait}>
              Me
            </Tab>
          </Box>
          <Tab
            isActive={activeTab === 'milestones'}
            resetFakeWait={resetFakeWait}
          >
            Milestones
          </Tab>
        </Row>
        <Box
          css={{
            backgroundColor: 'rgba(198,198,198)',
            p: 10,
            px: 15,
            mx: 10,
            mb: 10,
            boxShadow:
              '4px 4px 0px 0px #EBEBEB inset, -4px -4px 0px 0px #0A0A0A inset',
          }}
        >
          <Box css={{ maxWidth: 1000, mx: 'auto', mb: 20 }}>{Content()}</Box>
        </Box>
      </Box>
    </Box>
  );
}
