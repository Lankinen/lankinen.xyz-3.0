import { WindowsParagraph } from './Components';

export default function Me() {
  return (
    <>
      <WindowsParagraph>
        I enjoy spending my leisure time learning or applying my expertise to make life better for myself and
        others.
      </WindowsParagraph>
      <WindowsParagraph>
        The most effective way for me to move humanity forward is to code and
        build products/companies. I am experienced with many technologies but I
        also have a great mental model I can use to quickly master new tools.
      </WindowsParagraph>
      <WindowsParagraph>
        I prefer having control over what I do. I enjoy working in autonomous
        and fast phased teams with smart and hard working people.
      </WindowsParagraph>
      <WindowsParagraph>
        The end goal of working for others is to gather knowledge and
        connections to increase my odds of succeeding in my own ventures in the
        future.
      </WindowsParagraph>
    </>
  );
}
