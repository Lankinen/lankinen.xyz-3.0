import { Text } from '@lankinen/component-lab';
import { styled } from 'stitches.config';

export const WindowsText = styled(Text, {
  fontFamily: 'VT323, monospace',
});

export const WindowsParagraph = styled(WindowsText, {
  fontSize: 20,
  paddingTop: 10,
});

export const WindowsHeader = styled(WindowsText, {
  fontSize: 24,
  paddingTop: 15,
});
