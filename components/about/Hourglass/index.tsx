import base64hourglass from './base64hourglass';
import { Box } from '@lankinen/component-lab';
import { CSS } from 'stitches.config';

interface Props {
  size?: string | number;
  css?: CSS;
}

export default function Hourglass(props: Props) {
  const { size = 30, css, ...rest } = props;
  return (
    <Box
      css={{
        ...css,
        display: 'inline-block',
        width: size,
        height: size,
      }}
      {...rest}
    >
      <Box
        css={{
          display: 'block',
          background: base64hourglass,
          backgroundSize: 'cover',
          width: '100%',
          height: '100%',
        }}
      />
    </Box>
  );
}
