import { useState, useEffect } from 'react';

let to1: ReturnType<typeof setTimeout> | null = null;
let to2: ReturnType<typeof setTimeout> | null = null;

export default function useGhostWriter(message: [string, number][]) {
  const [helloText, setHelloText] = useState<string | undefined>();
  const [msg, setMsg] = useState('');
  const [endTime, setEndTime] = useState(100);
  const [pointer, setPointer] = useState(0);
  const [isDeleteMode, setIsDeleteMode] = useState(false);

  useEffect(() => {
    if (pointer < message.length) {
      setMsg(message[pointer][0]);
      setEndTime(Number(message[pointer][1]));
      if (helloText !== undefined) {
        to1 = setTimeout(() => {
          setIsDeleteMode(true);
          setHelloText(helloText.slice(0, -1));
        }, endTime);
      }
    }
    return () => {
      if (to1) {
        clearTimeout(to1);
      }
    };
  }, [pointer]);

  useEffect(() => {
    if (msg === '') {
      if (helloText === undefined) {
        setHelloText('');
      } else {
        setPointer(pointer + 1);
      }
      return () => null;
    }
    const wait = Math.floor(Math.random() * 75 + 10);
    to2 = setTimeout(
      () => {
        if (isDeleteMode && helloText) {
          if (helloText.length > 1) {
            setHelloText(helloText.slice(0, -1));
          } else {
            setIsDeleteMode(false);
            setHelloText(helloText.slice(0, -1));
          }
        } else {
          const nextChar = msg[0];
          setMsg(msg.slice(1));
          setHelloText(helloText + nextChar);
        }
      },
      isDeleteMode ? 20 : wait
    );
    return () => {
      if (to2) {
        clearTimeout(to2);
      }
    };
  }, [helloText]);

  return helloText;
}
