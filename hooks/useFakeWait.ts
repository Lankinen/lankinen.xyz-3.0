import { useState, useEffect, useCallback } from 'react';

let timeout: ReturnType<typeof setTimeout> | undefined;

export default function useFakeWait(seconds = 1000) {
  const [fakeWait, setFakeWait] = useState(true);

  const reset = useCallback(() => {
    if (timeout) {
      clearTimeout(timeout);
    }
    setFakeWait(true);
    timeout = setTimeout(() => {
      setFakeWait(false);
    }, seconds);
  }, [seconds]);

  useEffect(() => {
    reset();
    return () => timeout && clearTimeout(timeout);
  }, [reset]);

  return { fakeWait, resetFakeWait: reset };
}
