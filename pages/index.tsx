export async function getServerSideProps() {
  return {
    redirect: {
      destination: '/about',
      permanent: true,
    },
  };
}

function Home() {
  return null;
}

export default Home;
