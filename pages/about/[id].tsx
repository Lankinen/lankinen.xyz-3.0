import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import Container from 'components/about/Container';

const About: NextPage = () => {
  const router = useRouter();

  const { id: activeTab } = router.query;

  return <Container activeTab={activeTab as string} />;
};

export default About;
