import type { NextPage } from 'next';
import Container from 'components/about/Container';

const About: NextPage = () => {
  return <Container activeTab="me" />;
};

export default About;
