import type { NextPage } from 'next';
import AppIcon from 'components/projects/AppIcon';
import useWindowSize from 'hooks/useWindowSize';
import projectsData from 'data/projects/projects.json';
import { Box, Img, Row } from '@lankinen/component-lab';
import useFakeWait from '@/hooks/useFakeWait';
import { useRouter } from 'next/router';

const ICON_SIZE = 60;

const Projects: NextPage = () => {
  const windowSize = useWindowSize();
  const router = useRouter();

  const { fakeWait } = useFakeWait(750);

  const perRow = (() => {
    if (windowSize.width < 500) {
      return 2;
    } else if (windowSize.width < 800) {
      return 4;
    }
    return 6;
  })();

  return (
    <>
      <Row
        css={{
          backgroundColor: 'black',
          width: '100%',
          height: '100%',
          alignItems: 'center',
          display: fakeWait ? undefined : 'none',
          justifyContent: 'center',
        }}
      >
        <img
          src="/images/projects/apple.png"
          style={{ width: 50 }}
          alt="apple"
        />
      </Row>
      <Box
        css={{
          backgroundImage: 'url(/images/projects/background.png)',
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          height: '100%',
          visibility: fakeWait ? 'hidden' : undefined,
          position: fakeWait ? 'absolute' : undefined,
          left: fakeWait ? '-999em' : undefined,
        }}
      >
        <Box css={{ minHeight: '100%' }}>
          <Box
            css={{
              padding: 50,
              height: '100%',
              display: 'grid',
              gridTemplateColumns: `repeat(${perRow}, 1fr)`,
              gap: 50,
            }}
          >
            {projectsData.map((item) => (
              <AppIcon
                key={item.name}
                name={item.name}
                to={`/projects/${item.name}`}
                year={item.year}
                img={item.icon}
              />
            ))}
          </Box>
          <Row
            css={{
              position: 'absolute',
              bottom: 130,
              left: 0,
              right: 0,
              justifyContent: 'center',
            }}
          >
            <Row
              css={{
                backgroundColor: 'rgba(0,0,0,0.2)',
                maxWidth: 500,
                justifyContent: 'space-between',
                p: 15,
                gap: 15,
                borderRadius: 15,
              }}
            >
              {/* Mail */}
              <AppIcon
                to="mailto:elias@lankinen.xyz"
                img="apple-mail.png"
                size={ICON_SIZE}
              />
              {/* Notes */}
              <AppIcon
                to="https://lankinen.notion.site/Lankinen-92c1d0531acd48f99c4dcdd546ad1a68"
                img="apple-notes.png"
                size={ICON_SIZE}
              />
              {/* Photos */}
              <AppIcon
                to="https://www.behance.net/"
                img="apple-photos.png"
                size={ICON_SIZE}
              />
              {/* Music */}
              <AppIcon
                to="https://music.apple.com/us/album/saint-pablo/1443063578?i=1443063997"
                img="apple-music.png"
                size={ICON_SIZE}
              />
              {/* Books */}
              <AppIcon
                to="https://lankinen.notion.site/122eb3a37c044d0ca363a465f5724ace?v=f4f78e51ff004ff78c3beaf2fd60ba0b"
                img="apple-books.png"
                size={ICON_SIZE}
              />
            </Row>
          </Row>
          <Row
            css={{
              backgroundColor: 'black',
              position: 'absolute',
              bottom: 0,
              width: '100%',
              justifyContent: 'center',
            }}
          >
            <Img
              onClick={() => router.push('/')}
              src="/images/projects/home-button.png"
              alt="home button"
              css={{
                width: 90,
                height: 90,
                margin: 10,
                cursor: 'pointer',
              }}
            />
          </Row>
        </Box>
      </Box>
    </>
  );
};

export default Projects;
