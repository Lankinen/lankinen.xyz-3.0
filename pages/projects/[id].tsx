import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import projectsData from 'data/projects/projects.json';
import { Box, Row, Col, Img, Text, Span } from '@lankinen/component-lab';
import useFakeWait from 'hooks/useFakeWait';
import AppIcon from 'components/projects/AppIcon';

interface BottomFrameProps {
  position?: string;
}

function BottomFrame(props: BottomFrameProps) {
  const { position = 'sticky' } = props;
  const router = useRouter();

  return (
    <Row
      css={{
        backgroundColor: 'black',
        position,
        bottom: 0,
        width: '100%',
        justifyContent: 'center',
      }}
    >
      <Img
        onClick={() => router.back()}
        src="/images/projects/home-button.png"
        alt="home button"
        css={{
          width: 90,
          height: 90,
          margin: 10,
          cursor: 'pointer',
        }}
      />
    </Row>
  );
}

const BACKGROUND_COLOR = 'rgb(20,20,20)';

const Project: NextPage = () => {
  const router = useRouter();

  const { id } = router.query;

  const project = projectsData.find((item) => item.name === id);
  const { fakeWait } = useFakeWait();

  if (!project) {
    return null;
  }

  return (
    <>
      <Row
        css={{
          height: '100%',
          width: '100%',
          backgroundColor: BACKGROUND_COLOR,
          alignItems: 'center',
          display: fakeWait ? undefined : 'none',
          justifyContent: 'center',
        }}
      >
        <AppIcon img={project.icon} css={{ mb: 50 }} />
      </Row>
      <Box
        css={{
          height: '100%',
          visibility: fakeWait ? 'hidden' : undefined,
          position: fakeWait ? 'absolute' : undefined,
          left: fakeWait ? '-999em' : undefined,
        }}
      >
        <Row
          css={{
            backgroundColor: BACKGROUND_COLOR,
            minHeight: '100%',
            justifyContent: 'center',
          }}
        >
          <Col
            css={{
              maxWidth: 900,
              mx: 10,
              my: 10,
            }}
          >
            <>
              <Text as="h1" css={{ color: 'white' }}>
                {project.name}, {project.year}
              </Text>
              <Text css={{ color: 'white' }}>
                <Span
                  css={{
                    fontWeight: 'bold',
                  }}
                >
                  What:
                </Span>{' '}
                {project.what}
              </Text>
              <Text css={{ color: 'white' }}>
                <Span
                  css={{
                    fontWeight: 'bold',
                  }}
                >
                  Why:
                </Span>{' '}
                {project.why}
              </Text>
              <Row css={{ gap: 5, justifyContent: 'center' }}>
                {project.images.map((url: string) => (
                  <Img
                    key={url}
                    alt="product"
                    src={url}
                    css={{
                      maxHeight: 400,
                      maxWidth: '100%',
                      py: 10,
                    }}
                  />
                ))}
              </Row>
              <Text css={{ color: 'white' }}>
                <Span
                  css={{
                    fontWeight: 'bold',
                  }}
                >
                  Tools:
                </Span>{' '}
                {project.tech}
              </Text>
              <Text css={{ color: 'white' }}>
                <Span
                  css={{
                    fontWeight: 'bold',
                  }}
                >
                  Reason to quit:
                </Span>{' '}
                {project.quit}
              </Text>
              <Text css={{ color: 'white' }}>
                <Span
                  css={{
                    fontWeight: 'bold',
                  }}
                >
                  What I learned:
                </Span>{' '}
                {project.learnings}
              </Text>
              {project.urls?.map((url: string) => (
                <a href={url} key={url} target="_blank" rel="noreferrer">
                  {url}
                </a>
              ))}
            </>
          </Col>
        </Row>
        <BottomFrame />
      </Box>
    </>
  );
};

export default Project;
