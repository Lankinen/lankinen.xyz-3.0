import 'styles/globals.css';
import 'rc-tooltip/assets/bootstrap.css';
import { Box } from '@lankinen/component-lab';
import NavBar from 'components/NavBar';

function MyApp({ Component, pageProps }: { Component: any; pageProps: any }) {
  return (
    <>
      <NavBar height="5%" />
      <Box css={{ height: '95%' }}>
        <Component {...pageProps} />
      </Box>
    </>
  );
}

export default MyApp;
