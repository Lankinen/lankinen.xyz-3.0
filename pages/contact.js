import { useState } from 'react';
import Terminal from 'terminal-in-react';
import { commands, descriptions } from 'data/contact/commands';
import Image from 'next/image';
import { Box, Text } from '@lankinen/component-lab';
import { globalCss } from '@stitches/react';
import useWindowSize from 'hooks/useWindowSize';
import useGhostWriter from 'hooks/useGhostWriter';

const globalStyles = globalCss({
  'div::-webkit-scrollbar': {
    display: 'none',
  },
});

const mainMessage =
  "My email address is elias@lankinen.xyz but you can use this machine to send the email. Type 'help' to see all of the commands.";

const ghostMessage =
  "Don't hesistate to contact me! I enjoy reading all kind of emails. If you are a recruiter, could you customize your cold email.";

function Contact() {
  globalStyles();
  const [exited, setExited] = useState(false);

  const windowSize = useWindowSize();
  const ghostText = useGhostWriter([[ghostMessage, 5000]]);

  if (windowSize.width < 700) {
    return (
      <Box
        css={{
          width: '100%',
          height: '100%',
          backgroundColor: 'black',
          p: 10,
        }}
      >
        <Text css={{ color: 'green' }}>
          {!windowSize.width ? undefined : 'elias@lankinen.xyz'}
        </Text>
        <Text css={{ color: 'green' }}>{ghostText}</Text>
      </Box>
    );
  }
  if (exited) {
    return (
      <div
        style={{
          backgroundColor: 'black',
          width: '100%',
          height: '100%',
        }}
      >
        <Image
          alt="cat"
          loader={({ src }) => `images/contact/${src}`}
          src={'cat-middle-finger.jpeg'}
          height={300}
          width={200}
        />
      </div>
    );
  }
  return (
    <Box
      style={{
        height: '100%',
      }}
    >
      <Terminal
        style={{
          caretColor: 'black',
          fontWeight: 'bold',
          fontSize: '1.2em',
          height: '100%',
          width: '100%',
        }}
        commandPassThrough={(cmd, print) => {
          if (cmd.length === 1 && cmd[0] === 'exit') {
            setExited(true);
          } else {
            print("Type 'help' to see all of the commands");
          }
        }}
        hideTopBar
        allowTabs={false}
        startState="maximised"
        backgroundColor="black"
        barColor="black"
        commands={commands}
        descriptions={descriptions}
        msg={asciiMessage}
        promptSymbol="bash-3.2$"
      />
    </Box>
  );
}

const asciiMessage = ` 
88                           88         88                                        
88                           88         ""                                        
88                           88                                                   
88  ,adPPYYba,  8b,dPPYba,   88   ,d8   88  8b,dPPYba,    ,adPPYba,  8b,dPPYba,   
88  ""     \`Y8  88P'   \`"8a  88 ,a8"    88  88P'   \`"8a  a8P_____88  88P'   \`"8a  
88  ,adPPPPP88  88       88  8888[      88  88       88  8PP"""""""  88       88  
88  88,    ,88  88       88  88\`"Yba,   88  88       88  "8b,   ,aa  88       88  
88  \`"8bbdP"Y8  88       88  88   \`Y8a  88  88       88   \`"Ybbd8"'  88       88  

${mainMessage}`;

export default Contact;
