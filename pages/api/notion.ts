import { NextApiRequest, NextApiResponse } from 'next';
import { Client } from '@notionhq/client';

export interface Book {
  name: string;
  url: string;
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Book[]>
) {
  try {
    const notion = new Client({ auth: process.env.NOTION_KEY });
    const databaseId = '122eb3a37c044d0ca363a465f5724ace';

    const response = await notion.databases.query({
      database_id: databaseId,
      page_size: 5,
      filter: { property: 'Status', select: { equals: 'Read' } },
      sorts: [
        {
          property: 'Read date',
          direction: 'descending',
        },
      ],
    });

    const books = response.results.map((result: any) => {
      const name = result.properties.Name.title[0].plain_text;
      return { name, url: result.url };
    });
    return res.status(200).json(books);
  } catch {
    return res.status(500).json([]);
  }
}
