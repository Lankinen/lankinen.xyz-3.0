import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<number>
) {
  try {
    const response = await fetch(
      'https://api.twitter.com/1.1/users/show.json?user_id=1040262224735084545',
      {
        headers: {
          Authorization: process.env.TWITTER_AUTH_HEADER ?? '',
        },
      }
    );
    if (response.status !== 200) {
      throw Error;
    }
    const data = await response.json();
    const followerCount = Number(data['followers_count']);

    return res.status(200).send(followerCount);
  } catch {
    return res.status(500).send(0);
  }
}
