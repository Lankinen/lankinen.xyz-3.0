import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<string>
) {
  try {
    const response = await fetch(
      'https://api.twitter.com/2/users/1040262224735084545?expansions=pinned_tweet_id',
      {
        headers: {
          Authorization: process.env.TWITTER_AUTH_HEADER ?? '',
        },
      }
    );
    const data = await response.json();
    const id = data['includes']['tweets'][0]['id'];

    return res.status(200).send(id);
  } catch (error) {
    return res.status(500).send('1316631648960688130');
  }
}
