import { NextApiRequest, NextApiResponse } from 'next';
import { createClient } from '@supabase/supabase-js';

const key = process.env.SUPABASE_SECRET_KEY || '';

const supabase = createClient('https://azrgdbffykdrvsyemwbz.supabase.co', key);

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<string>
) {
  try {
    const { data } = await supabase
      .from('Calendars')
      .select()
      .order('week', { ascending: false })
      .limit(1)
      .single();
    return res.status(200).send(data.data);
  } catch {
    return res.status(500);
  }
}
