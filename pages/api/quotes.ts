import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<string>
) {
  try {
    const response = await fetch('https://type.fit/api/quotes');
    if (response.status !== 200) {
      throw Error;
    }
    const data = await response.json();
    const selected = data[Math.floor(Math.random() * data.length)];
    return res.status(200).send(`"${selected.text}" - ${selected.author}`);
  } catch {
    return res.status(500).json('"Time is money" - Benjamin Franklin');
  }
}
