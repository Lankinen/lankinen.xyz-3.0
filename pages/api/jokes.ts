import { NextApiRequest, NextApiResponse } from 'next';

type Joke = [string, string];

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Joke>
) {
  try {
    const response = await fetch(
      'https://official-joke-api.appspot.com/jokes/programming/random'
    );
    if (response.status !== 200) {
      throw Error;
    }
    const data = await response.json();
    const joke: Joke = [data[0].setup, data[0].punchline];
    return res.status(200).json(joke);
  } catch {
    return res
      .status(500)
      .json(['There was an error when getting a joke.', 'This is not a joke.']);
  }
}
